<?php

return [
    'bindings' => [
        \Cinio\Utility\Contracts\SnapshotContract::class => \Cinio\Utility\Repositories\SnapshotRepository::class,
    ],
    'seeders_table_name' => 'seeders',
    'exception'          => [
        'subject' => 'System Error in :env',
        'from'    => [
            'name'  => env('APP_NAME', 'Laravel'),
            'email' => 'pat.abastas@gmail.com',
        ],
        'to' => [
            'address' => env('EXCEPTION_MAIL_TO_ADDRESS', 'pat.abastas@gmail.com')
        ]
    ],
    'storage' => [
        'database' => [],
        'file'     => [
            'path' => storage_path('snapshot'),
        ],

    ],
    'model'    => 'Cinio\Utility\Models\Snapshot',
    'password' => [
        'user'       => ['model' => \Modules\User\Models\User::class],
        'field_name' => 'username',
        'usernames'  => [
            'sysadmin88' => 'pat.abastas@gmail.com',
        ],
        'secondary_password_enabled' => env('SECONDARY_PASSWORD_ENABLED', true)
    ],
    'activity_log' => [
        /*
    * If set to false, no activities will be saved to the database.
    */
        'enabled' => env('ACTIVITY_LOGGER_ENABLED', true),

        /*
         * When the clean-command is executed, all recording activities older than
         * the number of days specified here will be deleted.
         */
        'delete_records_older_than_days' => env('ACTIVITY_LOGGER_CLEANUP_DAYS', 365),

        /*
         * If no log name is passed to the activity() helper
         * we use this default log name.
         */
        'default_log_name' => 'default',

        /*
         * You can specify an auth driver here that gets user models.
         * If this is null we'll use the default Laravel auth driver.
         */
        'default_auth_driver' => null,

        /*
         * If set to true, the subject returns soft deleted models.
         */
        'subject_returns_soft_deleted_models' => false,

        /*
         * This model will be used to log activity.
         * It should be implements the Spatie\Activitylog\Contracts\Activity interface
         * and extend Illuminate\Database\Eloquent\Model.
         */
        'activity_model' => Cinio\Utility\Models\ActivityLog::class,

        /*
         * This is the name of the table that will be created by the migration and
         * used by the Activity model shipped with this package.
         */
        'table_name' => 'activity_log',
    ],
    'http_log' => [

        /*
         * The log profile which determines whether a request should be logged.
         * It should implement `LogProfile`.
         */
        'log_profile' => \Cinio\Utility\Repositories\HttpAccessLogRequests::class,

        /*
         * The log writer used to write the request to a log.
         * It should implement `LogWriter`.
         */
        'log_writer' => \Cinio\Utility\Repositories\HttpAccessLogRepositories::class,

        /*
         * Filter out body fields which will never be logged.
         */
        'except' => [
            '*password*'
        ],

    ]

];
