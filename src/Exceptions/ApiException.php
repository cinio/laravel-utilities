<?php

namespace Cinio\Utility\Exceptions;

use Cinio\Base\Response\ApiResponse;
use Exception;
use Illuminate\Http\JsonResponse;

class ApiException extends GeneralException
{
    use ApiResponse;

    protected $message;

    public function __construct(Exception $exception, $message = '')
    {
        parent::__construct($message, 0, null);
        $this->exception = $exception;
        $this->message   = empty($message)? __('s.oops something went wrong.'):$message;
    }
    
    /**
     * Render an exception into an HTTP response.
     *
     * @return JsonResponse
     */
    public function render()
    {
        return $this->respondErrors($this->message);
    }

    /**
     * Get the laravel exception
     *
     * @return  Exception
     */
    public function getException()
    {
        return $this->exception;
    }
}
