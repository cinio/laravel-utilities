<?php

namespace Cinio\Utility\Exceptions;

use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;

class WebException extends GeneralException
{
    protected $exception    = '';
    protected $url          = '';
    protected $errorMessage = '';

    public function __construct(Exception $exception)
    {
        parent::__construct('', 0, null);
        $this->exception = $exception;
    }
    
    /**
     * Render an exception into an HTTP response.
     *
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function render()
    {
        if (empty($this->url)) {
            abort(500);
        }

        return redirect($this->url)->with('error', $this->errorMessage);
    }

    public function redirectTo($url)
    {
        $this->url = $url;

        return $this;
    }

    public function withMessage($message)
    {
        $this->errorMessage = $message;

        return $this;
    }

    /**
     * Get the laravel exception
     *
     * @return Exception|string
     */
    public function getException()
    {
        return $this->exception;
    }
}
