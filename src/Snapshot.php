<?php


namespace Cinio\Utility;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use JsonSerializable;

class Snapshot implements Arrayable, Jsonable, JsonSerializable
{
    const DATABASE = 'database';

    const FILE = 'file';
    /**
     * The key
     * @var unknown
     */
    protected $key;

    protected $snapshot;

    protected $tempTableName;

    protected $model;

    /**
     * The data
     * @var unknown
     */
    protected $data = [];

    /**
     * Class constructor
     * @param unknown $key
     */
    public function __construct($key = null)
    {
        $this->model = resolve(config('utility.model'));
        if ($key) {
            $this->load($key);
        }
    }

    /**
     * Convert the model to its string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->toJson();
    }

    /**
     * Load data db
     * @param unknown $key
     * @return \Modules\Snapshot\Snapshot
     */
    public function load($key)
    {
        $this->key = $key;

        $snapshot = $this->model->where('key', $key)->first();
        if ($snapshot) {
            $this->snapshot = $snapshot;
            if (self::FILE == $snapshot->storage) {
                $fullpath = $snapshot->path . DIRECTORY_SEPARATOR . $snapshot->filename;
                if (file_exists($fullpath)) {
                    $contents   = file_get_contents($fullpath);
                    $this->data = json_decode($contents, true);
                }
            } else {
                $this->data = json_decode($snapshot->data, true);
            }
        }

        return $this;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Snapshot\Contracts\SnapshotContract::snapshotDB()
     */
    public function snapshotDB($table, $storage = 'database')
    {
        $rows = DB::table($table)->get();

        $contents = [];
        foreach ($rows as $row) {
            $contents[] = (array) $row;
        }

        unset($rows);

        return $this->snapshotArray($contents, $storage, $table);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Snapshot\Contracts\SnapshotContract::snapshotArray()
     */
    public function snapshotArray(array $data, $storage = 'database', $table = null, $identifier = null)
    {
        $this->validStorage($storage);
        $snapshot             = new $this->model;
        $snapshot->data       = json_encode($data);
        $snapshot->table      = $table;
        $snapshot->key        = $this->generateKey();
        $snapshot->identifier = $identifier;

        if ($storage == self::FILE) {
            $path = config('snapshot.storage.file.path');
            if (!file_exists($path)) {
                if (false === mkdir($path, 0777, true)) {
                    throw new \Exception('Unable to save the file ' . $path);
                }
            }

            $filename = $snapshot->key . $this->extension;
            $fullpath = $path . DIRECTORY_SEPARATOR . $filename;

            if (false === file_put_contents($fullpath, $snapshot->data)) {
                throw new \Exception('Unable to save the file ' . $fullpath);
            }

            $snapshot->data     = null;
            $snapshot->path     = $path;
            $snapshot->filename = $filename;
            $snapshot->storage  = self::FILE;
        }
        $result = $snapshot->save();

        return $result ? $snapshot->key : false;
    }

    /**
     * Get storages
     * @return array
     */
    public function getStorages()
    {
        return array_keys(config('snapshot.storage'));
    }

    /**
     * Valid storage
     * @param unknown $storage
     * @return boolean
     */
    protected function validStorage($storage)
    {
        if (!in_array($storage, $this->getStorages())) {
            throw new \Exception('Storage not supported, please see config file.');
        }

        return true;
    }

    /**
     * Return random string
     * @return string
     */
    protected function generateKey()
    {
        $randomString = str_random(5) . microtime(true);
        return md5($randomString);
    }

    /**
     * Get data
     * @return \Modules\Snapshot\unknown
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Get Key
     * @return \Modules\Snapshot\unknown
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Search a specific record from data
     * @param unknown $column
     * @param unknown $value
     * @return array
     */
    public function search($column, $value)
    {
        return array_filter($this->data, function ($val) use ($column, $value) {
            return $val[$column] == $value;
        });
    }

    /**
     * Convert the object to its JSON representation.
     *
     * @param  int  $options
     * @return string
     */
    public function toJson($options = 0)
    {
        $json = json_encode($this->jsonSerialize(), $options);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw JsonEncodingException::forModel($this, json_last_error_msg());
        }

        return $json;
    }

    /**
     * Convert the object into something JSON serializable.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->data;
    }

    /**
     * @param null $referenceTbl - Reference table of the snapshot
     * @param null $tempName - Table name of the temp table
     */
    public function toTempTable($tempTbl = null, $referenceTbl = null)
    {
        if (!$referenceTbl) {
            $referenceTbl = $this->snapshot->table;
        }
        if (!$tempTbl) {
            $tempTbl = $referenceTbl . '_temp';
        }

        $this->tempTableName = $tempTbl;

        DB::statement("DROP TEMPORARY TABLE IF EXISTS " . $tempTbl);

        DB::statement("CREATE TEMPORARY TABLE " . $tempTbl . " SELECT * FROM " . $referenceTbl . " LIMIT 0");
        foreach (array_chunk($this->data, 1000, true) as $data) {
            DB::table($tempTbl)->insert($data);
        }
    }

    public function dropTempTable($tempTableName = null)
    {
        if (!$tempTableName) {
            $tempTableName = $this->tempTableName;
        }

        if ($tempTableName) {
            DB::statement("DROP TEMPORARY TABLE IF EXISTS " . $tempTableName);
        }
    }
}
