<?php

namespace Cinio\Utility\Repositories;

use Illuminate\Support\Facades\DB;
use Cinio\Base\Repositories\Repository;
use Cinio\Utility\Contracts\SnapshotContract;
use Cinio\Utility\Models\Snapshot as SnapshotModel;
use Cinio\Utility\Snapshot;

class SnapshotRepository extends Repository implements SnapshotContract
{
    /**
     * Limit
     * @var integer
     */
    protected $limit = 5000;

    /**
     * The file extension
     * @var string
     */
    protected $extension = '.json';

    /**
     * Class constructor
     * @param SnapshotModel $model
     */
    public function __construct(SnapshotModel $model)
    {
        parent::__construct($model);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Snapshot\Contracts\SnapshotContract::snapshotDB()
     */
    public function snapshotDB($table, $storage = 'database')
    {
        $rows = DB::table($table)->get();

        $contents = [];
        foreach ($rows as $row) {
            $contents[] = (array) $row;
        }

        unset($rows);

        return $this->snapshotArray($contents, $storage, $table);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Snapshot\Contracts\SnapshotContract::snapshotArray()
     */
    public function snapshotArray(array $data, $storage = 'database', $table = null, $identifier = null)
    {
        $this->validStorage($storage);
        $snapshot             = new $this->model;
        $snapshot->data       = json_encode($data);
        $snapshot->table      = $table;
        $snapshot->key        = $this->generateKey();
        $snapshot->identifier = $identifier;

        if ($storage == Snapshot::FILE) {
            $path = config('snapshot.storage.file.path');
            if (!file_exists($path)) {
                if (false === mkdir($path, 0777, true)) {
                    throw new \Exception('Unable to save the file ' . $path);
                }
            }

            $filename = $snapshot->key . $this->extension;
            $fullpath = $path . DIRECTORY_SEPARATOR . $filename;

            if (false === file_put_contents($fullpath, $snapshot->data)) {
                throw new \Exception('Unable to save the file ' . $fullpath);
            }

            $snapshot->data     = null;
            $snapshot->path     = $path;
            $snapshot->filename = $filename;
            $snapshot->storage  = Snapshot::FILE;
        }
        $result = $snapshot->save();

        return $result ? $snapshot->key : false;
    }

    /**
     * Get storages
     * @return array
     */
    public function getStorages()
    {
        return array_keys(config('snapshot.storage'));
    }

    /**
     *
     * {@inheritDoc}
     * @see \Cinio\Modules\Snapshot\Contracts\SnapshotContract::get()
     */
    public function get($key)
    {
        $snapshot = $this->model->where('key', $key)->first();
        if ($snapshot) {
            return new Snapshot($key);
        }

        return;
    }

    /**
     * Valid storage
     * @param unknown $storage
     * @return boolean
     */
    protected function validStorage($storage)
    {
        if (!in_array($storage, $this->getStorages())) {
            throw new \Exception('Storage not supported, please see config file.');
        }

        return true;
    }

    /**
     * Return random string
     * @return string
     */
    protected function generateKey()
    {
        $randomString = str_random(5) . microtime(true);
        return md5($randomString);
    }
}
