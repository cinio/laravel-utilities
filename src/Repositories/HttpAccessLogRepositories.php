<?php

namespace Cinio\Utility\Repositories;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Spatie\HttpLogger\LogWriter;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class HttpAccessLogRepositories implements LogWriter
{
    public function logRequest(Request $request)
    {
        $method = strtoupper($request->getMethod());

        $uri = $request->getPathInfo();

        $bodyAsJson = json_encode($this->getLoggableParameters($request));

        // $files = array_map(function (UploadedFile $file) {
        //     return $file->path();
        // }, iterator_to_array($request->files));

        $files = [];

        $userId  = Auth::user()? Auth::user()->id: '';
        $message = "{$method} {$uri} - Body: {$bodyAsJson} - Files:  " . implode(', ', $files) . "- User: {$userId}";
        $message .= json_encode($request->header());

        return Log::channel('http')->info($message);
    }

    /**
     * Get parameters which can be logged based on config.
     *
     * @param Request $request
     * @return array
     */
    protected function getLoggableParameters(Request $request)
    {
        $exclusions = config('http-logger.except');
        $keys       = $request->keys();

        foreach ($exclusions as $exclusion) {
            $exclusion = str_replace('*', '.*', $exclusion);
            $keys      = preg_grep("/^$exclusion$/", $keys, PREG_GREP_INVERT);
        }

        return $request->only($keys);
    }
}
