<?php

namespace Cinio\Utility\Repositories;

use Illuminate\Http\Request;
use Spatie\HttpLogger\LogProfile;

class HttpAccessLogRequests implements LogProfile
{
    public function shouldLogRequest(Request $request): bool
    {
        return true;
    }
}
