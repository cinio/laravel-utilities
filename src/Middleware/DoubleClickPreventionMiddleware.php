<?php

namespace Cinio\Utility\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class DoubleClickPreventionMiddleware
{
    private $checkRequestMethodIs;

    public function __construct()
    {
        $this->checkRequestMethodIs = ['POST', 'PUT', 'PATCH', 'DELETE'];
    }
    
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws ValidationException
     */
    public function handle(Request $request, Closure $next)
    {
        if (!in_array($request->method(), $this->checkRequestMethodIs)) {
            return $next($request);
        }

        if ($request->session()->get('_form_token') === $request->get('_form_token')) {
            $request->session()->forget('_form_token');
            return $next($request);
        }
    
        throw ValidationException::withMessages([
            'double_click' => ['s_validation.your request has been processed'],
        ]);
    }
}
