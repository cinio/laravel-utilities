<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSnapshotsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('snapshots', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('identifier')->index()->nullable();
            $table->string('key')->unique();
            $table->string('table')->nullable()->index();
            $table->enum('storage', ['file', 'database'])->default('database');
            $table->string('path')->nullable();
            $table->string('filename')->nullable();
        });

        // Since laravel doesn't support this, need to do it like this
        DB::statement("ALTER TABLE snapshots ADD data LONGBLOB DEFAULT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('snapshots');
    }
}
