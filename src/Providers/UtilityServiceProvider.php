<?php

namespace Cinio\Utility\Providers;

use Illuminate\Support\ServiceProvider;
use Cinio\Utility\Console\SnapshotCommand;
use Cinio\Utility\Console\SysadminPasswordGenerateCommand;

class UtilityServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * The commands
     * @var array
     */
    protected $commands = [
        SnapshotCommand::class,
        SysadminPasswordGenerateCommand::class,
    ];

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerViews();
        $this->registerConfig();
        $this->registerBindings();
        $this->loadMigrationsFrom(__DIR__ . '/../Migrations');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->commands($this->commands);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['utility'];
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/plus65/utility');

        $sourcePath = __DIR__ . '/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/plus65/utility';
        }, \Config::get('view.paths')), [$sourcePath]), 'utility');
    }

    /**
     * bind interfaces to implementations
     * @return void
     */
    public function registerBindings()
    {
        foreach (config('utility.bindings') as $key => $binding) {
            $this->app->bind($key, $binding);
        }
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../../config/config.php' => config_path('utility.php'),
        ], 'config');

        $this->mergeConfigFrom(
            __DIR__ . '/../../config/config.php',
            'utility'
        );

        //set activity-log config
        $this->app['config']->set('activitylog', array_merge(empty(config('activitylog'))? []:config('activitylog'), config('utility')['activity_log']));

        //set http-log config
        $this->app['config']->set('http-logger', array_merge(empty(config('http-logger'))? []:config('http-logger'), config('utility')['http_log']));
    }
}
