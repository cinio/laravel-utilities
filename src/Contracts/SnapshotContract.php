<?php

namespace Cinio\Utility\Contracts;

interface SnapshotContract
{
    /**
     * Snapshot database table
     * @param unknown $table
     * @param string $storage
     * @return string
     */
    public function snapshotDB($table, $storage = 'database');
    
    /**
     * Snapshot data from array
     *
     * @param array $data
     * @param string $storage
     * @param null $table
     * @param null $identifier
     * @return string
     */
    public function snapshotArray(array $data, $storage = 'database', $table = null, $identifier = null);

    /**
     * Get the Snapshot
     * @param unknown $key
     * @return Cinio\Modules\Snapshot\Snapshot
     */
    public function get($key);
}
