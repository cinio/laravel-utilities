<?php

namespace Cinio\Utility\Models;

use Illuminate\Database\Eloquent\Model;

class Seeders extends Model
{
    public $guarded = [];
    protected $table;

    public function __construct(array $attributes = [])
    {
        $this->table = config('utility.seeder_table_name');

        parent::__construct($attributes);
    }
}
