<?php

namespace Cinio\Utility\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Snapshot extends Model
{
    use SoftDeletes;

    /**
     * Fillable columns
     * @var array
     */
    protected $fillable = [
        'key',
        'storage',
        'path',
        'data',
    ];
}
