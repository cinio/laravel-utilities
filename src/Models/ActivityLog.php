<?php

namespace Cinio\Utility\Models;

use Spatie\Activitylog\Models\Activity;

class ActivityLog extends Activity
{
    protected $connection = 'mysql_logs';
    protected $fillable   = [];
}
