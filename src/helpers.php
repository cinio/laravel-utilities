<?php

use Illuminate\Support\HtmlString;
use Illuminate\Support\Str;

if (!function_exists('form_token_field')) {
    /**
     * Generate a CSRF token form field.
     *
     * @return HtmlString
     */
    function form_token_field()
    {
        return new HtmlString('<input type="hidden" name="_form_token" value="' . form_token() . '">');
    }
}

if (!function_exists('form_token')) {
    function form_token()
    {
        $session = app('session');

        if (isset($session)) {
            $token = Str::random(32);
            $session->put('_form_token', $token);

            return $token;
        }

        throw new RuntimeException('Application session store not set.');
    }
}
