<?php

namespace Plus65\Utility\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewPassword extends Notification
{
    /**
     * The password
     *
     * @var string
     */
    protected $password;

    /**
     * The secondary password
     *
     * @var string
     */
    protected $secondaryPassword;

    /**
     * The username
     *
     * @var string
     */
    protected $username;

    /**
     * The email
     *
     * @var string
     */
    protected $email;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($username, $email, $password, $secondaryPassword = '')
    {
        $this->password          = $password;
        $this->secondaryPassword = $secondaryPassword;
        $this->username          = $username;
        $this->email             = $email;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $notifiable->email = $this->email;

        $mailMessage = (new MailMessage)
            ->subject('Password Reset For ' . $this->username . ' - ' . config('app.env'))
            ->greeting('Hello!')
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->line('Your password has been changed: ')
            ->line('Environment: ' . config('app.env'))
            ->line('Username: ' . $this->username)
            ->line('Password: ' . $this->password);

        if ($this->secondaryPassword) {
            $mailMessage = $mailMessage->line('Secondary Password: ' . $this->secondaryPassword);
        }

        return $mailMessage;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [

        ];
    }
}
