<?php

namespace Cinio\Utility\Console;

use Illuminate\Console\Command;
use Cinio\Utility\Snapshot;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class SnapshotCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'snapshot:take';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Snapshot database table data.';

    /**
     * The snapshot repository
     * @var unknown
     */
    protected $snapshot;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Snapshot $snapshot)
    {
        $this->snapshot = $snapshot;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $table   = $this->argument('table');
        $storage = $this->argument('storage');
        $option  = $storage ? $storage : $this->snapshot::DATABASE;

        if (!in_array($option, $this->snapshot->getStorages())) {
            $this->error('Invalid storage');
        } else {
            $key = $this->snapshot->snapshotDB($table, $option);
            if ($key) {
                $this->info('Snapshot for table ' . $table . ' is successful. The key is ' . $key);
            } else {
                $this->info('Snapshot for table ' . $table . ' failed.');
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['table', InputArgument::REQUIRED, 'The database table.'],
            ['storage', InputArgument::OPTIONAL, 'The snapshot storage, default database.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
