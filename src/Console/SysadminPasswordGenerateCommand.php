<?php

namespace Cinio\Utility\Console;

use Illuminate\Console\Command;
use Modules\User\Models\User;
use Cinio\Utility\Notifications\NewPassword;
use RandomLib\Factory;
use SecurityLib\Strength;
use Symfony\Component\Console\Input\InputArgument;

class SysadminPasswordGenerateCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'password:generate_sysadmin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate new passwords for sysadmin users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userModel       = null;
        $userNames       = null;
        $userModelConfig = config('auth.providers.users.models');
        $passwordConfig  = config('utility.password');
        $argUsernames    = $this->argument('usernames');

        /**
         * lets try to discover our user model
         */
        if (isset($passwordConfig['user']['model'])) {
            $userModel = resolve($passwordConfig['user']['model']);
        } elseif ($userModelConfig) {
            $userModel = resolve($userModelConfig);
        }

        if ($argUsernames) {
            $userNames = explode(',', $argUsernames);
        }

        if (empty($userNames) && isset($passwordConfig['usernames']) && !empty($passwordConfig['usernames'])) {
            $userNames = $passwordConfig['usernames'];
        }

        /**
         * if we don't have a user model
         * or we have no usernames to generate new passwords to
         * then there is no purpose in our existence
         */
        if (!$userModel || !$userNames) {
            return true;
        }

        foreach ($userNames as $username => $email) {
            $userModel = new User;
            $user      = $userModel->newQuery()->where($passwordConfig['field_name'], $username)->first();

            if ($user) {
                $password          = $this->random_string(8);
                $secondaryPassword = null;

                if ($passwordConfig['secondary_password_enabled']) {
                    $secondaryPassword        = $this->random_string(8);
                    $user->secondary_password = bcrypt($secondaryPassword);
                }

                $user->password = bcrypt($password);
                $user->save();

                $user->notify(new NewPassword($username, $email, $password, $secondaryPassword));
            }
        }
    }

    public function random_string($length = 30, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $factory   = new Factory;
        $generator = $factory->getGenerator(new Strength(Strength::LOW));
        return $generator->generateString($length, $keyspace);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['usernames', InputArgument::OPTIONAL, 'users usernames to reset password']
        ];
    }
}
